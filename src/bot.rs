use super::manuels::{get_all_manuels, Manuel};
use super::ThreadSafeError;
use super::{CONF_CLASSE_MESSAGE, CONF_ERR_CHANNEL, CONF_OPTS_MESSAGE, CONF_SPES_MESSAGE};
use regex::Regex;
use serenity::{async_trait, model::prelude::*, prelude::*};
use std::io::{Error, ErrorKind};
use std::{collections::HashMap};
use strsim::normalized_damerau_levenshtein;
macro_rules! unwrap_or_return_ok_none {
    ($e:expr) => {
        match $e {
            Some(value) => value,
            None => return Ok(None),
        }
    };
}

enum Command {
    Liste,
    Image(String, usize),
    UnknownManuel(String),
    SyntaxError,
}

fn range_from_page_str(page_str: &str) -> Result<impl Iterator<Item = usize>, ThreadSafeError> {
    let mut debut_fin = page_str.split('-');
    let debut_str = debut_fin.next();
    let fin_str = debut_fin.next();
    let debut;
    let fin;
    match (debut_str, fin_str) {
        (Some(debut_str), Some(fin_str)) => {
            debut = debut_str.parse::<usize>()?;
            fin = fin_str.parse::<usize>()?;
            if fin > debut + 10 {
                Ok(debut..=debut + 10)
            } else {
                Ok(debut..=fin)
            }
        }
        (Some(debut_str), None) => {
            debut = debut_str.parse::<usize>()?;
            Ok(debut..=debut)
        }
        _ => Err(Box::new(Error::new(
            ErrorKind::InvalidData,
            "invalid page range",
        ))),
    }
}
fn get_page_from_str(pages_str: &str) -> Result<Vec<usize>, ThreadSafeError> {
    Ok(pages_str
        .split_whitespace()
        .into_iter()
        .flat_map(range_from_page_str)
        .flatten()
        .collect())
}
pub struct Bot {
    user_id: UserId,
    manuels: HashMap<String, Manuel>,
    command_regex: Regex,
    reactions: HashMap<MessageId, HashMap<String, String>>,
}
impl Bot {
    pub fn new(user_id: UserId) -> Self {
        Self {
            user_id,
            manuels: get_all_manuels(),
            command_regex: Regex::new(
                r"!manuel(\s+((?P<commande>liste)|((?P<manuel>\w+)\s+(?P<pages>(\d+(-\d+)?\s+)*(\d+(-\d+)?)))))?",
            )
            .unwrap(),
            reactions: get_reactions_table(),
        }
    }
    async fn get_member_role_from_reaction(
        &self,
        ctx: &Context,
        reaction: &Reaction,
    ) -> Result<Option<(RoleId, Member)>, ThreadSafeError> {
        let unistr = match &reaction.emoji {
            ReactionType::Unicode(unistr) => unistr,
            _ => return Ok(None),
        };
        let table = unwrap_or_return_ok_none!(self.reactions.get(&reaction.message_id));
        let rolename = unwrap_or_return_ok_none!(table.get(unistr));
        let guild = unwrap_or_return_ok_none!(reaction.guild_id);
        let userid = unwrap_or_return_ok_none!(reaction.user_id);
        let user = guild.member(&ctx.http, userid).await?;
        let role =
            unwrap_or_return_ok_none!(guild.roles(&ctx.http).await?.values().find_map(|r| {
                if r.name.eq(rolename) {
                    Some(r.id)
                } else {
                    None
                }
            }));
        Ok(Some((role, user)))
    }
    fn get_commands_from_content(&self, content: &str) -> Result<Vec<Command>, ThreadSafeError> {
        let mut commands = Vec::new();
        for caps in self.command_regex.captures_iter(content) {
            match (
                caps.name("commande").map(|x| x.as_str()),
                caps.name("manuel").map(|x| x.as_str()),
                caps.name("pages").map(|x| x.as_str()),
            ) {
                (Some("liste"), None, None) => {
                    commands.push(Command::Liste);
                }
                (None, Some(input_manuel), Some(pages)) => {
                    let maybe_man = self.get_similar_manuel_name(input_manuel);
                    if let Some(valid_manuel) = maybe_man {
                        for page_str in pages.split_whitespace() {
                            for page in get_page_from_str(page_str)? {
                                commands.push(Command::Image(valid_manuel.to_owned(), page));
                            }
                        }
                    } else {
                        commands.push(Command::UnknownManuel(input_manuel.to_owned()));
                    }
                }
                _ => {
                    commands.push(Command::SyntaxError);
                }
            }
        }
        Ok(commands)
    }
    fn get_similar_manuel_name(&self, name: &str) -> Option<String> {
        if self.manuels.contains_key(name) {
            Some(name.to_string())
        } else {
            self.manuels
                .keys()
                .filter_map(|valid_name| {
                    let prob = normalized_damerau_levenshtein(valid_name, name);
                    if prob > 0.5 {
                        Some((valid_name, prob))
                    } else {
                        None
                    }
                })
                .max_by(|a, b| a.1.partial_cmp(&b.1).unwrap())
                .map(|(name, _prob)| name.to_string())
        }
    }
    async fn respond_message(
        &self,
        ctx: &Context,
        channel: ChannelId,
        manuel: &Manuel,
        page: usize,
    ) -> Result<(), ThreadSafeError> {
        match manuel.retrieve(page).await {
            Ok(filepath) => {
                println!(
                "sending to channel {}",
                channel
                    .name(&ctx.cache)
                    .await
                    .unwrap_or_else(|| "{unknown_channel_name}".to_string())
            );
            channel
                .send_message(&ctx.http, |m| m.add_file(filepath.to_str().unwrap_or_default()))
                .await?;
                }
        Err(_e) => {
            channel
                .say(
                    &ctx.http,
                    format!(
                        "Page introuvable. Pour info, le manuel a {} pages",
                        manuel.page_number()
                    ),
                )
                .await?;
            }
        }
        Ok(())
    }
    async fn handle_message(&self, ctx: &Context, msg: &Message) -> Result<(), ThreadSafeError> {
        if msg.author.id == self.user_id {
            return Ok(());
        }
        let commands = self.get_commands_from_content(&msg.content)?;
        for command in commands {
            match command {
                Command::UnknownManuel(invalid_name) => {
                    msg.channel_id
                        .say(
                            &ctx.http,
                            format!(
                                "Il existe pas ce manuel `{}`. Les manuels disponibles sont {}.",
                                invalid_name,
                                self.manuels
                                    .keys()
                                    .map(|s| s.as_str())
                                    .collect::<Vec<&str>>()
                                    .join(", ")
                            ),
                        )
                        .await?;
                }
                Command::SyntaxError => {
                    msg.channel_id
                        .say(
                            &ctx.http,
                            "syntaxe: `!manuel matière pages...` ou `!manuel liste`",
                        )
                        .await?;
                }
                Command::Liste => {
                    msg.channel_id
                        .say(
                            &ctx.http,
                            format!(
                                "Voici la liste des manuels disponibles: {}",
                                self.manuels
                                    .keys()
                                    .map(|s| s.as_str())
                                    .collect::<Vec<&str>>()
                                    .join(", ")
                            ),
                        )
                        .await?;
                }
                Command::Image(manuel_name, page) => {
                    let manuel = self.manuels.get(&manuel_name).unwrap();
                    self.respond_message(ctx, msg.channel_id, manuel, page)
                        .await?;
                }
            }
        }
        Ok(())
    }
    async fn handle_reaction_add(
        &self,
        ctx: &Context,
        reaction: &Reaction,
    ) -> Result<(), ThreadSafeError> {
        if let Some((role, mut user)) = self.get_member_role_from_reaction(ctx, reaction).await? {
            user.add_role(&ctx.http, role).await?;
        }
        Ok(())
    }
    async fn handle_reaction_remove(
        &self,
        ctx: &Context,
        reaction: &Reaction,
    ) -> Result<(), ThreadSafeError> {
        if let Some((role, mut user)) = self.get_member_role_from_reaction(ctx, reaction).await? {
            user.remove_role(&ctx.http, role).await?;
        }
        Ok(())
    }
    async fn report_error(&self, ctx: &Context, e: &ThreadSafeError) {
        println!("{}", e);
        CONF_ERR_CHANNEL
            .say(&ctx.http, e)
            .await
            .expect("Can't send message");
    }
}
fn get_reactions_table() -> HashMap<MessageId, HashMap<String, String>> {
    fn emoji_name_from_num(n: usize) -> String {
        format!("{}\u{fe0f}\u{20e3}", n)
    }

    let classe: HashMap<String, String> =
        HashMap::from_iter((1..=4).map(|n| (emoji_name_from_num(n), format!("TE{}", n))));
    let spes: HashMap<String, String> = HashMap::from_iter(
        "maths physique hggsp hlp ses musique svt nsi latin anglais"
            .split(' ')
            .enumerate()
            .map(|(n, spe)| (emoji_name_from_num(n), format!("spé {}", spe))),
    );
    let opts: HashMap<String, String> = HashMap::from_iter(
        "musique,mathex,euro allemand,latin,math comp"
            .split(',')
            .enumerate()
            .map(|(n, opt)| (emoji_name_from_num(n), format!("option {}", opt))),
    );
    HashMap::from([
        (CONF_CLASSE_MESSAGE, classe),
        (CONF_SPES_MESSAGE, spes),
        (CONF_OPTS_MESSAGE, opts),
    ])
}
#[async_trait]
impl EventHandler for Bot {
    async fn message(&self, ctx: Context, msg: Message) {
        if let Err(e) = self.handle_message(&ctx, &msg).await {
            self.report_error(&ctx, &e).await;
            msg.channel_id
                .say(&ctx.http, "Oops something went wrong")
                .await
                .expect("Can't send message");
        }
    }
    async fn reaction_add(&self, ctx: Context, reaction: Reaction) {
        if let Err(e) = self.handle_reaction_add(&ctx, &reaction).await {
            self.report_error(&ctx, &e).await;
        }
    }
    async fn reaction_remove(&self, ctx: Context, reaction: Reaction) {
        if let Err(e) = self.handle_reaction_remove(&ctx, &reaction).await {
            self.report_error(&ctx, &e).await;
        }
    }
    async fn ready(&self, _: Context, ready: Ready) {
        println!("connected as {}", ready.user.name);
    }
}
