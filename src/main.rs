#[macro_use]
extern crate rocket;
use std::{
    process::{Child, Command},
    time::SystemTime,
};

use bot::Bot;
use chrono::{DateTime, Utc};
use manuels::get_all_manuels;
use rocket::{
    http::{ContentType, Status},
    request::{FromRequest, Outcome, Request},
    response::{Responder, Response, Result as ResponseResult},
};
use serenity::{
    http::Http,
    model::id::{ChannelId, MessageId},
    Client,
};
use tokio::{fs::File, io::AsyncReadExt};

mod bot;
mod manuels;
mod webscreen;

macro_rules! some_or_return {
    ($e:expr, $v: expr) => {
        match $e {
            Some(inner) => inner,
            None => {
                return $v;
            }
        }
    };
}
macro_rules! ok_or_return {
    ($e:expr, $v:expr) => {
        match $e {
            Ok(inner) => inner,
            Err(_) => {
                return $v;
            }
        }
    };
}

type ThreadSafeError = Box<dyn std::error::Error + Send + Sync>;

const CONF_ERR_CHANNEL: ChannelId = ChannelId(886563989407031316);
const CONF_CLASSE_MESSAGE: MessageId = MessageId(887050606462767144);
const CONF_SPES_MESSAGE: MessageId = MessageId(887050615132401704);
const CONF_OPTS_MESSAGE: MessageId = MessageId(887050635810312232);

fn start_webdriver_instance() -> Result<Child, ThreadSafeError> {
    Ok(Command::new("chromedriver").arg("--port=4444").spawn()?)
}

enum HttpResponse {
    Image((ContentType, Vec<u8>)),
    Error(Status)
}
impl<'r> Responder<'r, 'static> for HttpResponse {
    fn respond_to(self, req: &Request) -> ResponseResult<'static> {
       match self{ 
            Self::Image(data) => Response::build_from(data.respond_to(req)?).ok(),
            Self::Error(status) => Response::build().status(status).ok()
        }
    }
}

#[derive(Debug, PartialOrd, PartialEq, Ord, Eq)]
struct HttpImageRequest(Option<DateTime<Utc>>);
#[async_trait]
impl<'r> FromRequest<'r> for HttpImageRequest {
    type Error = ();
    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        Outcome::Success(Self(
            request
                .headers()
                .get_one("If-Modified-Since")
                .map(|s| DateTime::parse_from_rfc2822(s).ok())
                .flatten()
                .map(|date| date.with_timezone(&Utc)),
        ))
    }
}

impl From<SystemTime> for HttpImageRequest {
    fn from(t: SystemTime) -> Self {
        Self(Some(t.into()))
    }
}

#[get("/<manuel>/<page>")]
async fn getimage(manuel: &str, page: usize, r: HttpImageRequest) -> HttpResponse {
    use HttpResponse::*;
    let all_manuels = get_all_manuels();
    let manuel = some_or_return!(all_manuels.get(manuel), Error(Status::NotFound));
    let path = ok_or_return!(manuel.retrieve(page).await, Error(Status::NotFound));
    let contenttype = ContentType::from_extension(manuel.getformat()).unwrap_or_default();
    let mut file = ok_or_return!(File::open(path).await, Error(Status::InternalServerError));
    if let Ok(meta) = file.metadata().await {
        if let Ok(lastmodified) = meta.modified() {
            if r > lastmodified.into() {
                return Error(Status::NotModified);
            }
        }
    }
    let mut data = Vec::new();
    if file.read_to_end(&mut data).await.is_err() {
        return Error(Status::InternalServerError);
    }
    return Image((contenttype, data));
}

#[rocket::main]
async fn main() {
    let token = std::env::var("DISCORD_TOKEN").expect("missing token");
    let http = Http::new_with_token(&token);
    let user_id = http
        .get_current_application_info()
        .await
        .expect("Couldn't get app info")
        .id;
    let bot = Bot::new(user_id);
    let mut client = Client::builder(&token)
        .event_handler(bot)
        .await
        .expect("Couldn't build client");
    start_webdriver_instance().expect("Couldn't start webdriver");
    tokio::task::spawn(async move { client.start().await.expect("Couldn't start client") });
    let rocket = rocket::build().mount("/", routes![getimage]);
    rocket.launch().await.expect("Can't start webserver");
}
