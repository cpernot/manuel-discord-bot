use super::webscreen::capture;
use super::ThreadSafeError;
use async_trait::async_trait;
use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};
use tokio::{
    fs::File,
    io::{AsyncReadExt, AsyncWriteExt, Error as IoErr, ErrorKind as IoErrKind},
};

#[async_trait]
trait Downloader: Sync {
    async fn fetch(&self, url: &str) -> Result<Vec<u8>, ThreadSafeError>;
}

struct WebDownloader;
#[async_trait]
impl Downloader for WebDownloader {
    async fn fetch(&self, url: &str) -> Result<Vec<u8>, ThreadSafeError> {
        println!("browser download of {}", url);
        capture(url).await
    }
}

struct HttpDownloader;
#[async_trait]
impl Downloader for HttpDownloader {
    async fn fetch(&self, url: &str) -> Result<Vec<u8>, ThreadSafeError> {
        println!("http request to {}", url);
        let resp = reqwest::get(url).await?;
        let data = resp.bytes().await?;
        Ok(Vec::from(&data as &[u8]))
    }
}

struct FileDownloader;
#[async_trait]
impl Downloader for FileDownloader {
    async fn fetch(&self, url: &str) -> Result<Vec<u8>, ThreadSafeError> {
        println!("reading file {}", url);
        let mut f = File::open(url).await?;
        let mut data = Vec::new();
        f.read_to_end(&mut data).await?;
        Ok(data)
    }
}

struct PageResolver {
    offset: usize,
    padding: usize,
    page_number: usize,
}
impl PageResolver {
    pub fn page_str(&self, page: usize) -> String {
        format!("{:01$}", page + self.offset, self.padding)
    }
}

trait Resolver: Sync {
    fn resolve(&self, page: usize) -> String;
    fn get_page_number(&self) -> usize;
}

pub struct IdResolver {
    base_url: String,
    id: String,
    page_resolver: PageResolver,
}
impl IdResolver {
    pub fn new(
        base_url: &str,
        id: &str,
        offset: usize,
        padding: usize,
        page_number: usize,
    ) -> Self {
        Self {
            base_url: base_url.to_owned(),
            id: id.to_owned(),
            page_resolver: PageResolver {
                offset,
                padding,
                page_number,
            },
        }
    }
}
impl Resolver for IdResolver {
    fn resolve(&self, page: usize) -> String {
        let url = self.page_resolver.page_str(page);

        self.base_url
            .replace("{page}", url.as_str())
            .replace("{id}", self.id.as_str())
    }
    fn get_page_number(&self) -> usize {
        self.page_resolver.page_number
    }
}

pub struct FileResolver {
    base_str: String,
    page_resolver: PageResolver,
}
impl FileResolver {
    pub fn new(base_str: &str, offset: usize, padding: usize, page_number: usize) -> Self {
        Self {
            base_str: base_str.to_owned(),
            page_resolver: PageResolver {
                offset,
                padding,
                page_number,
            },
        }
    }
}
impl Resolver for FileResolver {
    fn resolve(&self, page: usize) -> String {
        let url = self.page_resolver.page_str(page);
        self.base_str.replace("{page}", url.as_str())
    }
    fn get_page_number(&self) -> usize {
        self.page_resolver.page_number
    }
}

pub struct VallsResolver;
impl Resolver for VallsResolver {
    fn resolve(&self, page: usize) -> String {
        String::from("./valls.jpg")
    }

    fn get_page_number(&self) -> usize {
        0
    }
}

pub struct Manuel {
    name: String,
    resolver: Box<dyn Resolver + Send + Sync>,
    downloader: &'static dyn Downloader,
    format: &'static str,
    in_filesystem: bool,
}
impl Manuel {
    pub fn magnard(name: &str, id: &str, page_number: usize) -> Self {
        Self {
            name: name.to_owned(),
            resolver: Box::new(IdResolver::new(
                "https://storage.libmanuels.fr/Magnard/specimen/{id}/OEBPS/page{page}.xhtml",
                id,
                2,
                3,
                page_number,
            )),
            downloader: &WebDownloader,
            format: "png",
            in_filesystem: false,
        }
    }
    pub fn calameo(name: &str, id: &str, offset: usize, page_number: usize) -> Self {
        Self {
            name: name.to_owned(),
            resolver: Box::new(IdResolver::new(
                "https://p.calameoassets.com/{id}/p{page}.jpg",
                id,
                offset,
                0,
                page_number,
            )),
            downloader: &HttpDownloader,
            format: "jpg",
            in_filesystem: false,
        }
    }
    pub fn hachette1(name: &str, id: &str, page_number: usize) -> Self {
        Self {
            name: name.to_owned(),
            resolver: Box::new(IdResolver::new(
                "https://exobank.hachette-livre.fr/contents/final/{id}-fxl/OEBPS/Page_{page}.html",
                id,
                0,
                0,
                page_number,
            )),
            downloader: &WebDownloader,
            format: "png",
            in_filesystem: false,
        }
    }
    pub fn hachette2(name: &str, id: &str, page_number: usize) -> Self {
        Self {
            name: name.to_owned(),
            resolver: Box::new(IdResolver::new(
                "https://exobank.hachette-livre.fr/contents/final/{id}-fxl/OEBPS/page{page}.xhtml",
                id,
                0,
                0,
                page_number,
            )),
            downloader: &WebDownloader,
            format: "png",
            in_filesystem: false,
        }
    }
    pub fn maths(name: &str) -> Self {
        Self {
            name: name.to_owned(),
            resolver: Box::new(FileResolver::new(
                "data/sesamath/spemathtle-{page}.png",
                2,
                3,
                516,
            )),
            downloader: &FileDownloader,
            format: "png",
            in_filesystem: true,
        }
    }
    pub fn valls(name: &str) -> Self {
        Self {
            name: name.to_owned(),
            resolver: Box::new(VallsResolver),
            downloader: &FileDownloader,
            format: "jpg",
            in_filesystem: true,
        }
    }
    fn get_cache_path(&self, page: usize) -> PathBuf {
        PathBuf::from(format!("./cache/{}-{}.{}", self.name, page, self.format))
    }
    pub async fn retrieve(&self, page: usize) -> Result<PathBuf, ThreadSafeError> {
        if self.in_filesystem {
            return Ok(PathBuf::from(&self.resolver.resolve(page)));
        }
        let cachepath = self.get_cache_path(page);
        if !cachepath.exists() {
            self.download(page, &cachepath).await?;
        }
        Ok(cachepath)
    }
    async fn download(&self, page: usize, dest: &Path) -> Result<(), ThreadSafeError> {
        if page == 0 || page > self.page_number() {
            return Err(Box::new(IoErr::from(IoErrKind::NotFound)));
        }
        let url = self.resolver.resolve(page);
        let data = self.downloader.fetch(url.as_str()).await?;
        let updir = Path::new(&dest).parent().unwrap();
        tokio::fs::create_dir_all(updir).await?;
        let mut cachefile = File::create(&dest).await?;
        cachefile.write_all(&data).await?;
        Ok(())
    }
    pub fn getformat(&self) -> &str {
        self.format
    }
    pub fn page_number(&self) -> usize {
        self.resolver.get_page_number()
    }
}
unsafe impl Send for Manuel {}

pub fn get_all_manuels() -> HashMap<String, Manuel> {
    HashMap::from(
        [
            Manuel::magnard("histoire", "9782210114418/17", 339),
            Manuel::magnard("geo", "9782210114357/12", 323),
            Manuel::magnard("anglais", "9782210114029/19", 340),
            Manuel::calameo(
                "physique",
                "200831152507-99c183a66adc7dae17d836461cf5556d",
                2,
                528,
            ),
            Manuel::calameo(
                "philo",
                "200330171613-ca326a0db0cd16d4028669b383f69453",
                2,
                562,
            ),
            Manuel::calameo(
                "mathcomp",
                "200330174931-420cc2621af55a58bedd200ce12b09e7",
                2,
                292,
            ),
            Manuel::calameo(
                "svt",
                "200421182207-cc60de80f877a8f3d00e6c06545e142d",
                18,
                530,
            ),
            Manuel::hachette1("allemand", "9782016290200", 228),
            Manuel::hachette2("enseignement_sci", "9782401073418", 311),
            Manuel::maths("maths"),
            Manuel::valls("valls"),
        ]
        .map(|man| (man.name.to_owned(), man)),
    )
}
