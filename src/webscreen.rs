use crate::ThreadSafeError;
use fantoccini::{Client, ClientBuilder, Locator};
use serde_json::json;

const SCRIPT: &str =
    r#"return {"width": document.body.scrollWidth, "height":document.body.scrollHeight};"#;

async fn new_browser_instance() -> Result<Client, ThreadSafeError> {
    let arg = json!({"goog:chromeOptions":{"args":["-headless"]}})
        .as_object()
        .unwrap()
        .to_owned();

    Ok(ClientBuilder::native()
        .capabilities(arg)
        .connect("http://localhost:4444")
        .await?)
}

pub async fn capture(url: &str) -> Result<Vec<u8>, ThreadSafeError> {
    let mut c = new_browser_instance().await?;
    c.goto(url).await?;
    let jsondata = c.execute(SCRIPT, Vec::new()).await?;
    let width = jsondata["width"].as_u64().unwrap_or_default();
    let height = jsondata["height"].as_u64().unwrap_or_default();
    c.set_window_size(
        width.try_into().unwrap_or_default(),
        height.try_into().unwrap_or_default(),
    )
    .await?;
    let body = c.find(Locator::Css("body")).await?;
    let data = c.screenshot_element(body).await?;
    c.close().await?;
    Ok(data)
}
